extern crate cpp_demangle;

use std::io;
use std::io::Write;
use cpp_demangle::Symbol;
use std::fs::File;
use std::path::Path;

fn main() {
    let logpath = Path::join(&std::env::home_dir().unwrap(), "cppfilt_log.txt");
    let mut maybe_logfile = File::create(logpath);
    loop {
        let mut buffer = String::new();
        match io::stdin().read_line(&mut buffer) {
            Ok(n) => {
                match maybe_logfile {
                    Ok(ref mut f) => {
                        let logline = format!("{} - {}\n", n, buffer);
                        f.write_all(logline.as_bytes()).unwrap();
                        f.sync_all().unwrap();
                    },
                    Err(_) => {}
                }
                buffer.pop();
                let symbol = Symbol::new(&buffer[..]);
                match symbol {
                    Ok(s) => {
                        let symbol_text: String = format!("{}", s);
                        if symbol_text.len() > 0 {
                            match writeln!(io::stdout(), "{}", symbol_text) {
                                Err(e) => {
                                    writeln!(io::stderr(), "unable to write symbol to stdout: {} {}", buffer, e).unwrap();
                                },
                                Ok(_) => {}
                            }
                            io::stdout().flush().unwrap();
                        } else {
                            println!("{}", buffer);
                        }
                    },
                    Err(_) => {
                        writeln!(io::stdout(), "{}", buffer).expect("unable to decode symbol");
                        io::stdout().flush().unwrap();
                    },
                }
            },
            Err(err) => {
                let errmsg = writeln!(&mut io::stderr(), "Unable to read line");
                errmsg.expect("unable to write to stderr");
            }
        }
    }
}
