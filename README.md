In Windows 10, some symbols cause libiberty to return a 0 length symbol name from cppfilt. This causes Adobe Scout CC to hang, waiting for input.

This library is a from-scratch reimplementation of the cppfilt.exe binary in the Adobe Scout CC package for windows. 

Let me know if you find it useful.
